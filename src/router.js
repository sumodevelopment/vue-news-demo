import Vue from 'vue'
import VueRouter from 'vue-router'
import NewsConfigure from './components/NewsConfigure.vue'
import NewsList from './components/NewsList.vue'

Vue.use(VueRouter) // Add the Router features to the Vue Object

const routes = [
    {
        path: '/configure',
        alias: '/',
        component: NewsConfigure
    },
    {
        path: '/news',
        component: NewsList
    },
    {
        path: '/news/:country/:category',
        component: NewsList 
    }
    // TODO: Lazy loading
]

export default new VueRouter({ routes })