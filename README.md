# Vue News
A Vue app using the NewsAPI.

## API Documentation
https://newsapi.org/

## Features
- Styling
    - Bootstrap (With some Custom CSS)
- Components
    - Configure News - /
        - category
            business, entertainment, general, health, science, sports, technology
        - country
            - us USA, no Norway, uk United Kingdon, za South Africa, se Sweden
    - News List - /news
        - read from local storage
        - fetch the news with our config (country, category)
        - read our API key from the env
    - NewsListCard
        - card style list of news